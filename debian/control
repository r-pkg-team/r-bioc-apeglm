Source: r-bioc-apeglm
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-summarizedexperiment,
               r-bioc-genomicranges,
               r-cran-rcpp,
               r-cran-rcppeigen,
               r-cran-emdbook
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-apeglm
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-apeglm.git
Homepage: https://bioconductor.org/packages/apeglm/
Rules-Requires-Root: no

Package: r-bioc-apeglm
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Approximate posterior estimation for GLM coefficients
 apeglm provides Bayesian shrinkage estimators for effect sizes for a
 variety of GLM models, using approximation of the posterior for
 individual coefficients.
